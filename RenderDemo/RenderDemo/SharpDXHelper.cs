﻿using SharpDX;
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;

namespace RenderDemo
{
    /// <summary>
    /// SharpDX操作类
    /// </summary>
    public class SharpDXHelper
    {
        #region 属性
        /// <summary>
        /// D2D工厂
        /// </summary>
        private SharpDX.Direct2D1.Factory D2DFactory;

        /// <summary>
        /// 渲染目标
        /// </summary>
        private WicRenderTarget D2DRenderTarget;

        private RenderTarget _renderTarget;
        #endregion

        #region 构造
        public SharpDXHelper(Window window)
        {
            //像素模式和Alpha模式
            PixelFormat pixelFormat = new PixelFormat(SharpDX.DXGI.Format.Unknown, AlphaMode.Unknown);

            //渲染对象属性
            RenderTargetProperties renderTargetProperties = new RenderTargetProperties(RenderTargetType.Default, pixelFormat, 0, 0, RenderTargetUsage.None, FeatureLevel.Level_DEFAULT);
            
            //句柄渲染对象属性
            var hwndRenderTargetProperties = new HwndRenderTargetProperties();
            hwndRenderTargetProperties.Hwnd = new WindowInteropHelper(window).Handle;
            hwndRenderTargetProperties.PixelSize = new Size2((int)window.ActualWidth, (int)window.ActualHeight);


            _renderTarget = new WindowRenderTarget(D2DFactory, renderTargetProperties, hwndRenderTargetProperties);


        }
        #endregion

        #region 方法
        public void Clear()
        {
            this.D2DRenderTarget.BeginDraw();
            //this.D2DRenderTarget.Clear(SharpDX.Color.Transparent);
            this.D2DRenderTarget.EndDraw();
        }
        #endregion
    }
}
