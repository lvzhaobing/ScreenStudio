﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;

namespace RenderDemo
{

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        #region 属性
        private DispatcherTimer timer = null;

        private bool isPlaying = false;
        private bool mouseDown = false;


        #endregion

        #region 构造

        public MainWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region 方法
        //播放按钮
        private void button_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Stop();

            string aviFile = OpenFile();
            if (!string.IsNullOrWhiteSpace(aviFile))
            {
                TB_Path.Text = aviFile;
            }
        }

        private void mediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            sliderPosition.ValueChanged += this.sliderPosition_ValueChanged;
            sliderPosition.PreviewMouseDown += this.sliderPosition_MouseDown;
            sliderPosition.PreviewMouseUp += this.sliderPosition_MouseUp;

            if (mediaElement.NaturalDuration.HasTimeSpan)
                sliderPosition.Maximum = mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
            if (timer != null)
            {
                timer.Tick -= this.timer_tick;
                timer.Stop();
                timer = null;
            }
            isPlaying = false;
            playButton_Click();
        }

        /// <summary>
        /// 时间进度调鼠标抬起事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sliderPosition_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDown = false;
            Console.WriteLine("sliderPosition_MouseUp");
        }


        /// <summary>
        /// 时间进度条按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sliderPosition_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseDown = true;
            Console.WriteLine("sliderPosition_MouseDown");
        }

        private void timer_tick(object sender, EventArgs e)
        {
            if (!mouseDown)
            {
                sliderPosition.Value = mediaElement.Position.TotalSeconds;
            }
        }

        private void SetPoint(double totalSeconds)
        {
            DrawingVisual drawingVisual = new DrawingVisual();
        }

        //控制视频的位置
        private void sliderPosition_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (mouseDown)
            {
                mediaElement.Pause();
                mediaElement.Position = TimeSpan.FromSeconds(sliderPosition.Value);
                Thread.Sleep(1);
                mediaElement.Play();
            }
        }
        #endregion

        /// <summary>
        /// 打开文件
        /// </summary>
        /// <returns></returns>
        private string OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择文件";
            openFileDialog.Filter = "视频文件|*.avi;*.mp4;*.wmv;*.mkv;*.flv;*.mpg";
            openFileDialog.FileName = string.Empty;
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.DefaultExt = "zip";
            DialogResult result = openFileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.Cancel)
            {
                return null;
            }
            string fileName = openFileDialog.FileName;
            return fileName;
        }

        private void playButton_Click(object sender = null, RoutedEventArgs e = null)
        {
            //List<Point> points = new List<Point>();
            //points.Add(new Point(10, 10));
            //points.Add(new Point(20, 20));
            //points.Add(new Point(30, 30));
            //points.Add(new Point(40, 40));
            //points.Add(new Point(50, 50));
            //points.Add(new Point(60, 60));
            //ImageElement.Render(points);
            if (!isPlaying)
            {
                if (!string.IsNullOrWhiteSpace(TB_Path.Text))
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(10);
                    timer.Tick += timer_tick;
                    timer.Start();
                    mediaElement.Play();
                }
            }
            else
            {
                if (timer != null)
                {
                    timer.Stop();
                    timer.Tick -= timer_tick;
                    timer = null;
                }
                mediaElement.Pause();
            }
            isPlaying = !isPlaying;
            this.playButton.Content = isPlaying ? "暂停" : "播放";
        }
    }
}
