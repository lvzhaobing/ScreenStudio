﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DataModels;
using LinqToDB;
using LinqToDB.Data;

namespace Common
{
    public class DBManager
    {
        #region 单例
        private static object lock_obj = new object();
        private static DBManager instance;
        public static DBManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lock_obj)
                    {
                        if (instance == null)
                            instance = new DBManager();
                    }
                }
                return instance;
            }
        }
        #endregion

        #region 构造
        private DBManager()
        {

        }
        #endregion

        /// <summary>
        /// 根据记录名称查询记录
        /// </summary>
        /// <param name="recordingName"></param>
        /// <returns></returns>
        public List<RecordingData> GetRecordings(string recordingName = "")
        {
            List<RecordingData> recordings = null;
            using (var db = new DataBaseDB())
            {
                if (string.IsNullOrWhiteSpace(recordingName))
                {
                    recordings = (from item in db.RecordingDatas
                                  select item).ToList<RecordingData>();
                }
                else
                {
                    recordings = (from item in db.RecordingDatas
                                  where item.RecordingName == recordingName
                                  select item).ToList<RecordingData>();
                }

            }
            return recordings;
        }

        /// <summary>
        /// 插入或更新
        /// </summary>
        /// <param name="recordingData"></param>
        public void SaveRecording(RecordingData recordingData)
        {
            if (recordingData != null && !string.IsNullOrWhiteSpace(recordingData.RecordingName))
            {
                using (var db = new DataBaseDB())
                {
                    db.Insert(recordingData);
                }
            }
        }

        /// <summary>
        /// 移除记录
        /// </summary>
        /// <param name="recordingName"></param>
        public void RemoveRecordingData(string recordingName)
        {
            if (!string.IsNullOrWhiteSpace(recordingName))
            {
                using (var db = new DataBaseDB())
                {
                    var recording = (from item in db.RecordingDatas
                                     where item.RecordingName == recordingName
                                     select item).FirstOrDefault();
                    db.Delete(recording);
                }
            }
        }

        /// <summary>
        /// 查询特定时间段的注视点
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public List<Point> GetPoints(long startTime, long endTime)
        {
            List<Point> points = null;
            using (var db = new DataBaseDB())
            {
                points = (from item in db.Points
                          where item.TimeStamp >= startTime && item.TimeStamp <= endTime
                          orderby item.TimeStamp
                          select item).ToList();
            }
            return points;
        }

        /// <summary>
        /// 写入注视点数据
        /// </summary>
        /// <param name="point"></param>
        public void SavePoint(Point point)
        {
            if (point != null)
            {
                using (var db = new DataBaseDB())
                {
                    db.Insert(point);
                }
            }
        }

        /// <summary>
        /// 写入注视点数据
        /// </summary>
        /// <param name="point"></param>
        public void SavePoints(List<Point> points)
        {
            if (points != null && points.Count > 0)
            {
                Console.WriteLine("Insert Point Data Count: " + points.Count);
                using (var db = new DataBaseDB())
                {
                    db.BulkCopy(points);
                }
            }
        }

        /// <summary>
        /// 删除注视点
        /// </summary>
        /// <param name="timeStamp"></param>
        public void RemovePoint(long timeStamp)
        {
            using (var db = new DataBaseDB())
            {
                var point = (from item in db.Points
                             where item.TimeStamp == timeStamp
                             select item).FirstOrDefault();
                db.Delete(point);
            }
        }

        /// <summary>
        /// 删除注视点集合
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        public void RemovePoints(long startTime, long endTime)
        {
            if (endTime >= startTime)
            {
                using (var db = new DataBaseDB())
                {
                    var points = (from item in db.Points
                                 where item.TimeStamp >= startTime && item.TimeStamp <= endTime
                                 select item).ToList();
                    db.Delete(points);
                }
            }
        }
    }
}
