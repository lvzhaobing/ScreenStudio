﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Common.Control.MediaPlayer
{
    /// <summary>
    /// 视频播放器
    /// </summary>
    public class VideoPlayer : MediaElement
    {
        #region 属性

        public bool IsPlaying { get; set; }

        public double MaxDuration { get; set; }

      
        private DispatcherTimer timer;
        private bool isChanging;
        private bool mediaOpened;
        #endregion

        #region 委托
        public event Action<double> OnPositionChanged;
        public event Action<double> OnVideoOpened;
        #endregion

        /// <summary>
        /// 打开文件
        /// </summary>
        /// <param name="uri"></param>
        public void Open(string uri)
        {
            base.LoadedBehavior = MediaState.Manual;
            mediaOpened = false;
            base.MediaFailed += OnMediaFailed;
            base.MediaOpened += OnMediaOpened;
            base.MediaEnded += OnMediaEnded;
            if (!string.IsNullOrWhiteSpace(uri))
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(10);
                timer.Tick += Timer_Tick;
                base.Source = new Uri(uri);
            }
            base.Play();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (!isChanging)
            {
                OnPositionChanged?.Invoke(base.Position.TotalSeconds);
            }
        }

        private void OnMediaEnded(object sender, RoutedEventArgs e)
        {
            IsPlaying = false;
            if (timer != null)
            {
                timer.Stop();
            }
        }

        private void OnMediaFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void OnMediaOpened(object sender, RoutedEventArgs e)
        {
            mediaOpened = true;
            IsPlaying = true;
            if (timer != null)
            {
                timer.Start();
            }
            double time = 0;
            if (base.NaturalDuration.HasTimeSpan)
                time = base.NaturalDuration.TimeSpan.TotalSeconds;
            OnVideoOpened?.Invoke(time);
        }

        /// <summary>
        /// 开始播放
        /// </summary>
        public new void Play()
        {
            base.Play();
        }

        /// <summary>
        /// 暂停播放
        /// </summary>
        public new void Pause()
        {
            base.Pause();

        }

        /// <summary>
        /// 停止播放
        /// </summary>
        public new void Stop()
        {
            base.Stop();
        }

        /// <summary>
        /// 设置播放位置
        /// </summary>
        /// <param name="ts"></param>
        public void SetPosition(TimeSpan ts)
        {
            if (mediaOpened)
            {
                isChanging = true;
                base.Pause();
                base.Position = ts;
                Thread.Sleep(1);
                base.Play();
                base.Pause();
                isChanging = false;
            }
        }
    }
}
