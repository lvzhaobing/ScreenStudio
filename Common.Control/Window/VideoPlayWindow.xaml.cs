﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Common.Control.Window
{
    /// <summary>
    /// VideoPlayWindow.xaml 的交互逻辑
    /// </summary>
    public partial class VideoPlayWindow : System.Windows.Window
    {

        private bool mouseDown;

        public VideoPlayWindow()
        {
            InitializeComponent();
        }

        public VideoPlayWindow(string uri)
        {
            InitializeComponent();
            this.Open(uri);
        }

        public void Play()
        {
            videoPlayer.Play();
        }

        public void Pause()
        {
            videoPlayer.Pause();
        }

        public void Stop()
        {
            videoPlayer.Stop();
        }

        public void Open(string uri)
        {
            videoPlayer.OnVideoOpened += this.OnVideoOpened;
            videoPlayer.OnPositionChanged += this.OnPositionChanged;
            videoPlayer.Open(uri);

        }

        private void OnVideoOpened(double time)
        {
            this.positionSlider.Dispatcher.BeginInvoke((Action)(()=> {
                positionSlider.Maximum = time;
            }));
        }

        private void OnPositionChanged(double val)
        {
            positionSlider.Value = val;
        }

        /// <summary>
        /// 媒体文件已打开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void videoPlayerMediaOpend(object sender, RoutedEventArgs e)
        {

        }

        private void PositionSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (mouseDown)
                videoPlayer.SetPosition(TimeSpan.FromSeconds(((Slider)sender).Value));
        }

        private void PositionSlider_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseDown = true;
        }

        private void PositionSlider_OnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            mouseDown = false;
        }
    }
}
